package br.unifil.dc.lab2.Algoritmos;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Classificadores {

    /**
     * Classifica a lista em ordem crescente, pelo método de
     * mergesort, in-place. Utiliza memória auxiliar.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     */
    public static void mergesort(List<Integer> lista) {
        // Casos base
        if (lista.size() <= 1) return;

        // Casos de subdivisão recursiva
        final int idxMeioLista = lista.size() / 2;
        List<Integer> esquerda = lista.subList(0, idxMeioLista);
        mergesort(esquerda);

        List<Integer> direita = lista.subList(idxMeioLista, lista.size());
        mergesort(direita);

        merge(lista, esquerda, direita);
    }

    private static void merge(List<Integer> lista, List<Integer> esquerda, List<Integer> direita) {
        List<Integer> merged = new ArrayList<>(lista);

        int idxE = 0, idxD = 0, idxL = 0;
        while (idxE < esquerda.size() && idxD < direita.size()) {
            if (esquerda.get(idxE) < direita.get(idxD)) {
                merged.set(idxL, esquerda.get(idxE));
                idxE++;
            } else {
                merged.set(idxL, direita.get(idxD));
                idxD++;
            }
            idxL++;
        }

        int idxF;
        List<Integer> faltantes;
        if (idxE < esquerda.size()) {
            faltantes = esquerda;
            idxF = idxE;
        } else {
            faltantes = direita;
            idxF = idxD;
        }

        while (idxF < faltantes.size()) {
            merged.set(idxL, faltantes.get(idxF));
            idxL++; idxF++;
        }

        for (int i = 0; i < lista.size(); i++) {
            lista.set(i, merged.get(i));
        }
    }

    /**
     * Classifica a lista em ordem crescente, pelo método de
     * inserção, in-place.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     */
    public static void insertionsort(List<Integer> lista) {
        resetMedidores();

        for (int i = 1; i < lista.size(); i++) {
            Integer elem = lista.get(i);

            int j = i; comparacoes++;
            while (j > 0 && lista.get(j-1) > elem) {
                operacaoRwMemoria++;
                lista.set(j, lista.get(j-1)); // Deslocamento

                j--; comparacoes++;
            }

            lista.set(j, elem);
        }
    }

    public static <Modelo extends Comparable<Modelo>> void bubblesortCrescente(List<Modelo> lista) {
        bubblesort(lista, Classificadores::maiorQue);
    }
    public static <Modelo extends Comparable<Modelo>> void bubblesortDeCrescente(List<Modelo> lista) {
        bubblesort(lista, Classificadores::menorQue);
    }

    /**
     * Classifica a lista em ordem crescente, pelo método de
     * bubblesort, in-place.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     * @param comparador Função de comparação para correta ordenação.
     */
    public static <TElem> void bubblesort(List<TElem> lista, BiFunction<TElem, TElem, Boolean> comparador) {
        resetMedidores();

        boolean houvePermuta; do {
            houvePermuta = false;

            // Sobe a bolha
            for (int i = 1; i < lista.size(); i++) {
                comparacoes++;
                if (comparador.apply(lista.get(i - 1), lista.get(i))) {
                    permutar(lista, i - 1, i);
                    houvePermuta = true;
                }
            }
        } while (houvePermuta);
    }

    public static <Modelo extends Comparable<Modelo>> boolean maiorQue(Modelo l, Modelo r) {
        return l.compareTo(r) > 0;
    }

    public static <Modelo extends Comparable<Modelo>> boolean menorQue(Modelo l, Modelo r) {
        return l.compareTo(r) < 0;
    }

    /**
     * Classifica a lista em ordem crescente, pelo método de
     * selection sort, in-place.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     */
    public static void selectionsort(List<Integer> lista) {
        resetMedidores();

        for (int i = 0; i < lista.size(); i++) {
            int menorIdx = encontrarIndiceMenorElem(lista, i);
            permutar(lista, menorIdx, i);
        }
    }

    public static void bogosort(List<Integer> lista) {
        Random rng = new Random();
        while (!isOrdenada(lista)) {
            int a = rng.nextInt(lista.size());
            int b = rng.nextInt(lista.size());
            permutar(lista, a, b);
        }
    }

    public static String prettyPrintMedicoes() {
        return "Houve " + comparacoes + " comparacoes, e " + operacaoRwMemoria + " operacoes RW em memoria.";
    }


    /********
     *  SECAO DOS PRIVATES
     */

    private static boolean isOrdenada(List<Integer> lista) {
        for (int i = 1; i < lista.size(); i++)
            if (lista.get(i-1) > lista.get(i))
                return false;

        return true;
    }

    /**
     * Encontra o índice do menor elemento da lista.
     * @param lista lista a ser procurada.
     * @return índice do elemento, na escala iniciada em zero.
     */
    private static int encontrarIndiceMenorElem(List<Integer> lista, int idxInicio) {
        int menor = idxInicio;
        for (int i = idxInicio+1; i < lista.size(); i++) {
            comparacoes++;
            if (lista.get(menor) > lista.get(i))
                menor = i;
        }
        return menor;
    }

    /**
     * Permuta (swap) dois elementos da lista de posição.
     * @param lista Lista cujos elementos serão permutados.
     * @param a Îndice do primeiro elemento a ser permutado.
     * @param b Îndice do outro elemento a ser permutado.
     */
    private static <TElem> void permutar(List<TElem> lista, int a, int b) {
        TElem permutador = lista.get(a); // permutador = lista[a]
        lista.set(a, lista.get(b)); // lista[a] = lista[b]
        lista.set(b, permutador); // lista[b] = permutador

        operacaoRwMemoria+=2;
    }

    private static void resetMedidores() {
        operacaoRwMemoria = 0;
        comparacoes = 0;
    }

    // NUNCA FACA ISSO PROFISSIONALMENTE!!!
    private static int operacaoRwMemoria = 0;
    private static int comparacoes = 0;
}
