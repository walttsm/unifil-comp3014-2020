package br.unifil.dc.lab2.Colecoes;

public class Opcional<T> {

    public static Opcional vazio() { return Opcional.valorVazio; }

    public static <E> Opcional<E> de(E valor) {
        if (valor == null) throw new RuntimeException("Tentativa de criar opcional vazio.");
        return new Opcional<>(valor);
    }

    public T get() {
        if (valor == null) throw new RuntimeException("Tentativa de acessar opcional vazio.");
        return valor;
    }

    public boolean isVazio() {
        return valor == null;
    }

    @Override
    public String toString() {
        return "Opcional("+(isVazio() ? "" : valor )+")";
    }

    /****
     * Metodos e atributos privados
     */
    private static Opcional valorVazio = new Opcional();

    private Opcional(T valor) {
        this.valor = valor;
    }

    private Opcional() {
        this.valor = null;
    }

    private T valor;
}
