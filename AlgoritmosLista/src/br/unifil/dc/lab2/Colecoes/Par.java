package br.unifil.dc.lab2.Colecoes;

public class Par<T, U> {
    public Par(T esquerda, U direita) {
        this.esquerda = esquerda;
        this.direita = direita;
    }

    public T getEsquerda() {
        return esquerda;
    }

    public U getDireita() {
        return direita;
    }

    @Override
    public String toString() {
        return "Par("+getEsquerda()+","+getDireita()+")";
    }

    private T esquerda;
    private U direita;
}

class Resultado<Erro, Valor> extends Par<Opcional<Erro>, Opcional<Valor>> {

    public static <E> Resultado erro(E e) {
        return new Resultado(Opcional.de(e), Opcional.vazio());
    }

    public static <V> Resultado de(V v) {
        return new Resultado(Opcional.vazio(), Opcional.de(v));
    }

    public boolean isResultado() {
        return getEsquerda().isVazio();
    }

    public boolean isErro() {
        return !isResultado();
    }

    public Erro getErro() {
        return getEsquerda().get();
    }

    public Valor get() {
        return getDireita().get();
    }

    private Resultado(Opcional<Erro> e, Opcional<Valor> v) {
        super(e,v);
    }
}

class Ponto2D<T, U> extends Par<T, U> {
    public Ponto2D(T x, U y) {
        super(x,y);
    }

    public T getX() {
        return getEsquerda();
    }

    public U getY() {
        return getDireita();
    }

    @Override
    public String toString() {
        return "Ponto2D("+getEsquerda()+","+getDireita()+")";
    }
}