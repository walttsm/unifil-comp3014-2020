package br.unifil.dc.lab2;

import br.unifil.dc.lab2.Colecoes.Opcional;
import br.unifil.dc.lab2.Colecoes.Par;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        System.out.println(Opcional.vazio());
        System.out.println(Opcional.de(3020));

        Par<Float, Float> ponto2D = new Par<>(2.3f,0.1f);
        System.out.println(ponto2D);

        Par<String, Integer> resultado = new Par<>("Valor não encontrado",3);
        System.out.println(resultado);
    }


    public static Resultado<String, Integer> pesquisaLinear(List<Integer> vals, Integer chave) {
        for (int i = 0; i < vals.size(); i++) {
            if (chave.equals(vals.get(i)))
                return Resultado.de(i);
        }

        return Resultado.erro("Não há chave em valores.");
    }

    private static void referenciasEscondidas() {
        List<Integer> valores = Arrays.asList(1,2,3,4);
        List<Integer> valoresMeio = valores.subList(1,3);
        System.out.println(valores +" |+| "+valoresMeio);

        valoresMeio.set(0,77);
        System.out.println(valores +" |+| "+valoresMeio);
    }

    private static void exercicioMutabilidade() {
        List<Integer> listaMutavel = Arrays.asList(1,2,3);
        listaMutavel.set(0,0); // Mutação
        System.out.println(listaMutavel);

        List<Integer> listaImutavel = List.of(1,2,3);
        listaImutavel.set(0,0); // Mutação
        System.out.println(listaImutavel);
    }

    private static void exercicioRng() {
        Random rng = new Random("Aula do Ricardo".hashCode());
        for (int i = 0; i < 10; i++) {
            System.out.println(rng.nextInt(10_000));
        }
    }
}
